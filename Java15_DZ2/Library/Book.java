package SolutionSberuniversity.java_dz3.part2.library;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Book {

    private final String name;
    private final String author;
    private StatusBook status;
    private String nameOfVisitor;
    private Integer passportOfVisitor;
    private final Map<Visitor, Integer> mapOfGradesVisitors = new HashMap<>();

    Book(String name, String author) {
        this.status = StatusBook.newBook;
        this.name = name;
        this.author = author;
    }

    public String getName() {
        return this.name;
    }

    public String getAuthor() {
        return this.author;
    }

    public StatusBook getStatus() { return this.status; }

    public void setVisitor(String name, Integer passport) {
        if (this.nameOfVisitor == null && this.passportOfVisitor == null) {
            this.nameOfVisitor = name;
            this.passportOfVisitor = passport;
            this.status = StatusBook.lent;
        } else {
            System.out.println("the book is currently being read by < " + nameOfVisitor + " > < " + passport + " > ");
        }
    }

    public void returnBook() {
        if (this.status == StatusBook.lent) {
            this.status = StatusBook.returned;
            this.nameOfVisitor = null;
            this.passportOfVisitor = null;
        } else {
            System.out.println("book in the library");
        }
    }

    public void landBook() {
        if (this.status != StatusBook.lent) {
            this.status = StatusBook.lent;
//            this.nameOfVisitor = nameOfVisitor;
        } else {
            System.out.println("book lent by " + this.getNameOfVisitor());
        }
    }

    public String getNameOfVisitor() {
        return this.nameOfVisitor;
    }

    public Integer getPassportOfVisitor() {
        return this.passportOfVisitor;
    }

    public void setGradeOfVisitors(Visitor visitor, Integer grade) {
        this.mapOfGradesVisitors.put(visitor, grade);
    }

    public double getAVGGrades(Book book) {
        if (mapOfGradesVisitors.isEmpty()) { return 0; }

        int tempSumGrades = 0;
        for (Visitor visitor : mapOfGradesVisitors.keySet()) {
            tempSumGrades += mapOfGradesVisitors.get(visitor);
        }

        return ((int)(tempSumGrades * 1.0 / mapOfGradesVisitors.keySet().size()) * 10) / 10.0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return getName().equals(book.getName()) &&
                getAuthor().equals(book.getAuthor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getAuthor(), getStatus(), getNameOfVisitor(), mapOfGradesVisitors);
    }
}

