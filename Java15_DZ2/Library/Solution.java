package SolutionSberuniversity.java_dz3.part2.library;

public class Solution {
    public static void main(String[] args) {

        // create a library
        Library nlibrary = new Library();

        // add books
        nlibrary.addBookToLibrary(new Book("petrBook", "petr"));
        nlibrary.addBookToLibrary(new Book("petrBook", "petr")); // try add duplicate Book
        nlibrary.addBookToLibrary(new Book("abdullaBook", "abdulla"));
        nlibrary.addBookToLibrary(new Book("vasyaBook", "vasya"));
        nlibrary.addBookToLibrary(new Book("kolyaBook", "kolya"));
        nlibrary.addBookToLibrary(new Book("ninaBook", "nina"));
        nlibrary.addBookToLibrary(new Book("ninaBook1", "nina"));
        nlibrary.addBookToLibrary(new Book("ninaBook2", "nina"));
        nlibrary.addBookToLibrary(new Book("zinaBook", "zina"));
        nlibrary.addBookToLibrary(new Book("berdaBook", "berda"));

        // check all books
        System.out.println();
        nlibrary.printAllBooks();

        // try delete books if incorrect name
        System.out.println();
        nlibrary.deleteBookFromLibrary("vasyabook", "vasya");

        // try delete book
        System.out.println();
        nlibrary.deleteBookFromLibrary("vasyaBook", "vasya");
        System.out.println();
        nlibrary.printAllBooks();

        // try delete if already deleted
        System.out.println();
        nlibrary.deleteBookFromLibrary("vasyaBook", "vasya");

        // try get Author of Book name (correct name author)
        System.out.println(nlibrary.getBook("kolyaBook").getAuthor());

        // try get Author of Book name (uncorrect name author)
        System.out.println(nlibrary.getBook("kolyabook").getAuthor());

        // try find books by Author
        nlibrary.findListOfBooksByAuthor("nina").forEach(s -> System.out.print(s.getName() + " "));
        System.out.println();
        nlibrary.findListOfBooksByAuthor("zina").forEach(s -> System.out.print(s.getName() + " "));
        System.out.println();
        nlibrary.findListOfBooksByAuthor("Gina").forEach(s -> System.out.print(s.getName() + " "));

        // try create new Visitors
        Visitor vis1 = new Visitor("aSergey", 101412412);
        Visitor vis2 = new Visitor("bSergey", 101123123);
        Visitor vis3 = new Visitor("cSergey", 101235235);
        Visitor vis4 = new Visitor("dSergey", 101356356);

        // try add Visitor to List of Visitors
        nlibrary.addVisitor(vis1);
        nlibrary.addVisitor(vis2);
        nlibrary.addVisitor(vis3);
        nlibrary.addVisitor(vis4);

        // try add duplicate Visitor
        // first way to add
        System.out.println();
        nlibrary.addVisitor(vis1);
        // second way to add
        System.out.println();
        nlibrary.addVisitor(new Visitor("aSergey", 101412412));

        // try print All Visitors;
        System.out.println();
        nlibrary.printAllVisitors();

        // try lend and return book to Visitor / out Visitor with grade
        System.out.println();
        nlibrary.lendBookToVisitor( "petrBook", "bSergey", 101123123);
        nlibrary.returnBookFromVisitor("petrBook", "bSergey", 101123123,5);
        System.out.println();
        nlibrary.lendBookToVisitor( "ninaBook", "bSergey", 101123123);
        nlibrary.returnBookFromVisitor("ninaBook", "bSergey", 101123123,4);
        System.out.println();
        nlibrary.lendBookToVisitor( "ninaBook", "aSergey", 101412412);
        nlibrary.returnBookFromVisitor("ninaBook", "aSergey", 101412412,2);
        System.out.println();

        // try lend and return book to Visitor / out Visitor without grade
        System.out.println();
        nlibrary.lendBookToVisitor( "petrBook", "bSergey", 101123123);
        nlibrary.returnBookFromVisitor("petrBook", "bSergey", 101123123);
        System.out.println();
        nlibrary.lendBookToVisitor( "ninaBook", "bSergey", 101123123);
        nlibrary.returnBookFromVisitor("ninaBook", "bSergey", 101123123);
        System.out.println();
        nlibrary.lendBookToVisitor( "ninaBook", "aSergey", 101412412);
        nlibrary.returnBookFromVisitor("ninaBook", "aSergey", 101412412);
        System.out.println();

        // trying to lent a book that is lented
        System.out.println();
        nlibrary.lendBookToVisitor( "petrBook", "bSergey", 101123123);
        nlibrary.returnBookFromVisitor("petrBook", "aSergey", 101412412);
        System.out.println();

        // try return book with uncorrect VisitorName
        System.out.println();
        nlibrary.returnBookFromVisitor("petrBook", "cQergey", 101235235, 5);

        // try return book wiyh uncorrect BookName
        System.out.println();
        nlibrary.returnBookFromVisitor("petrbook", "cSergey", 101235235,5);

        // try return book with correct Book and Visitor names and Status = Returned
        System.out.println();
        nlibrary.returnBookFromVisitor("petrBook", "bSergey", 101123123,5);

        // try print AVG grades of books
        System.out.println();
        System.out.print("AVG grades petrBook = ");
        nlibrary.printAVGGradesOfBook(nlibrary.getBook("petrBook"));
        System.out.print("AVG grades ninaBook = ");
        nlibrary.printAVGGradesOfBook(nlibrary.getBook("ninaBook"));
        System.out.print("AVG grades berdaBook = ");
        nlibrary.printAVGGradesOfBook(nlibrary.getBook("berdaBook"));

        System.out.println();
        System.out.println(nlibrary);

    }

}
