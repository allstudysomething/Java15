package SolutionSberuniversity.java_dz3.part2.library;

import java.util.Objects;

public class Visitor {

    private final String name;
    private final Integer passport;
    private Integer id;
    private String nameOfCurrentListenedBook;
    private static Integer count = 0;

    Visitor(String name, Integer passport) {
        this.name = name;
        this.passport = passport;
    }

    public String getName() {
        return this.name;
    }

    public Integer getPassport() {
        return this.passport;
    }

    public String getNameOfListenedBook() {
        return this.nameOfCurrentListenedBook;
    }

    public void setNameOfListenedBook(Book book) {
        if (book == null) {
            this.nameOfCurrentListenedBook = null;
        } else {
            this.nameOfCurrentListenedBook = book.getName();
        }
    }

    public Integer getId() {
        return this.id;
    }

    // TESTED
    public void setId() {
        if (this.id == null) {
            this.id = ++count;
        } else {
            System.out.println("Visitor's ID already exists");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Visitor)) return false;
        Visitor visitor = (Visitor) o;
        return getName().equals(visitor.getName()) &&
                getPassport().equals(visitor.getPassport());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPassport());
    }
}
