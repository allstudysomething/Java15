package SolutionSberuniversity.java_dz3.part2.library;

import java.util.ArrayList;
import java.util.List;

public class Library {

    private static final List<Book> listOfBooks = new ArrayList<>();
    private final List<Visitor> listOfVisitors = new ArrayList<>();

    // 1 of Tasks -> Add Book to Library TESTED
    // Edit this method. Methods equals and hashCode already overrided in class Book
    public void addBookToLibrary(Book book) {
        if (listOfBooks.contains(book)) {
            System.out.println("Book : < " + book.getName() + " >, author : < " + book.getAuthor() +
                    " > is already contained in the library"); return;
        }
        listOfBooks.add(book);
    }

    // 2 of Tasks -> Delete Book from Library TESTED
    public void deleteBookFromLibrary(String name, String author) {
        for (int i = 0; i < listOfBooks.size(); i++) {
            if(listOfBooks.get(i).getName().equals(name) && listOfBooks.get(i).getAuthor().equals(author)
                    && listOfBooks.get(i).getStatus() != StatusBook.lent) {
                Book tempBook = listOfBooks.get(i);
                listOfBooks.remove(tempBook);
                System.out.println("Book < " + tempBook.getName() + " >, author < " + tempBook.getAuthor() + " > deleted from library");
                return;
            }
        }
        System.out.println("Book < " + name + " >, author < " + author + " > not presented in library");
    }

    // 3 of Tasks -> Find Book and Return (Return Empty Book if book bot presented in library) TESTED
    public Book getBook(String name) {
        for (int i = 0; i < listOfBooks.size(); i++) {
            if (listOfBooks.get(i).getName().equals(name)) return listOfBooks.get(i);
        }
        System.out.println("Book < " + name + " > not finded in library");
        return new Book("","");
    }

    // 4 of Tasks -> Find List of Books of Author (return Empty Arraylits if Books by Author noy presented TESTED
    // Edit method fori to forEach
    public List<Book> findListOfBooksByAuthor(String author) {
        List<Book> arrBook = new ArrayList<>();
        for (Book b : listOfBooks) {
            if (b.getAuthor().equals(author)) arrBook.add(b);
        }

        if (arrBook.size() == 0) {
            System.out.println("Books by author < " + author + " > not presented in library");
        }

        return arrBook;
    }

    // 5 of Tasks -> Lend the Book to a Visitor TESTED
    public void lendBookToVisitor(String nameBook, String nameVisitor, Integer passport) {
        if (nameBook == null && nameVisitor == null) {
            System.out.println("Book name or Visitor name is null");
            return;
        }

        Book tempBook = getBook(nameBook);
        Visitor tempVisitor = getVisitor(nameVisitor, passport);

        if (!tempBook.getName().equals("") && tempBook.getStatus() != StatusBook.lent && tempBook.getNameOfVisitor() == null
                && !tempVisitor.getName().equals("") && tempVisitor.getNameOfListenedBook() == null) {
            tempVisitor.setId();
            tempVisitor.setNameOfListenedBook(tempBook);
            tempBook.setVisitor(tempVisitor.getName(), tempVisitor.getPassport());
            tempBook.landBook();
                System.out.println("Book " + tempBook.getName() + " -> Visitor " + tempBook.getNameOfVisitor()
                        + " " + tempVisitor.getPassport() + ", ID " + tempVisitor.getId());
        } else {
            System.out.println("it is impossible to issue a book " + tempBook.getName() + " by Visitor "
                    + tempVisitor.getName() + " " + tempVisitor.getPassport());
        }
    }

    // 6 of Tasks -> Return book into library TESTED
    public void returnBookFromVisitor(String nameBook, String nameVisitor, Integer passport, Integer grade) {
        if (nameBook == null && nameVisitor == null && passport == null) {
            System.out.println("Book name or Visitor name is null");
            return;
        }

        Book tempBook = getBook(nameBook);
        Visitor tempVisitor = getVisitor(nameVisitor, passport);
        if (tempBook.getStatus() == StatusBook.lent && tempBook.getNameOfVisitor().equals(tempVisitor.getName())
                && tempBook.getPassportOfVisitor().equals(passport)) {
            tempBook.returnBook();
            tempVisitor.setNameOfListenedBook(null);

            // 8 of Tasks -> Add grade with returning Book from Visitor (default 0) TESTED
            // added if for null grade
            if (grade != null) setGradeOfBook(tempBook, tempVisitor, grade);
            System.out.println("Completed : Book " + tempBook.getName() + " returned to library from Visitor " + tempVisitor.getName()
                    + " " + tempVisitor.getPassport());
        }
        else {
                System.out.println("Not to be able to return the book " + nameBook + " from visitor " + nameVisitor + " "
                        + passport + ", StatusBook is " + tempBook.getStatus());
        }
    }

    // My added method 7 Return book into library without grade (default grade 0)
    public void returnBookFromVisitor(String nameBook, String nameVisitor, Integer passport) {
        this.returnBookFromVisitor(nameBook, nameVisitor, passport, null);
    }

    // My added method 6
    public void setGradeOfBook(Book book, Visitor visitor, Integer grade) {
        book.setGradeOfVisitors(visitor, grade);
    }

    // My added method 5
    public void printAVGGradesOfBook(Book book) {
        System.out.println(book.getAVGGrades(book));
    }

    // My method 4
    public void addVisitor(Visitor visitor) {
        if (!listOfVisitors.contains(visitor)) {
            listOfVisitors.add(visitor);
        } else {
            System.out.println("Visitor < " + visitor.getName() + " > < " + visitor.getPassport() + " > is already exists");
        }
    }

    // My added method 3
    // Edit method. Replaced forI to forEach
    public void printAllVisitors() {
        System.out.println("Visitors list: ");
        for (Visitor v : listOfVisitors) {
            System.out.println(v.getName() + " " + v.getPassport());
        }
        System.out.println();
    }

    // My added method 2
    // Edit method. Replaced forI to forEach
    public Visitor getVisitor(String nameVisitor, Integer passport) {
        for (Visitor v : listOfVisitors) {
            if (v.getName().equals(nameVisitor) && v.getPassport().equals(passport)) return v;
        }
        System.out.println("Visitor < " + nameVisitor + " > not finded in library");
        return  new Visitor("", 0);
    }

    // My added method 1
    // Edit method. Replaced forI to forEach
    public void printAllBooks() {
        System.out.println("COUNT of Books = " + listOfBooks.size());
        for (Book b : listOfBooks) {
            System.out.print(b.getName() + " ");
        }
        System.out.println();
    }

    @Override
    public String toString() {
        System.out.println("Library contains : \n");

        for (Book b : listOfBooks) {
            System.out.println("Book : " + b.getName() + ", author : " + b.getAuthor());
        }
        System.out.println("COUNT of Books = " + listOfBooks.size());
        System.out.println();
        System.out.println("Visitors list: ");
        for (Visitor v : listOfVisitors) {
            System.out.println("Name : " + v.getName() + ", passport : " + v.getPassport());
        }
        System.out.println();
        return "";
    }

}
