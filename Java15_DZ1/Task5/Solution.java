package SolutionSberuniversity.java_dz3.part1.task5;

public class Solution {

    public static void main(String[] args) {

        DayOfWeek[] dowMass = new DayOfWeek[7];
        dowMass[0] = new DayOfWeek((byte)1, "Monday");
        dowMass[1] = new DayOfWeek((byte)2, "Tuesday");
        dowMass[2] = new DayOfWeek((byte)3, "Wednesday");
        dowMass[3] = new DayOfWeek((byte)4, "Thursday");
        dowMass[4] = new DayOfWeek((byte)5, "Friday");
        dowMass[5] = new DayOfWeek((byte)6, "Saturday");
        dowMass[6] = new DayOfWeek((byte)7, "Sunday");

        for (int i = 0; i < dowMass.length; i++) {
            System.out.println(dowMass[i].getDayOfWeek() + ", " + dowMass[i].getNameOfWeek());
        }
    }

}
