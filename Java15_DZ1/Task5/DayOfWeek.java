package SolutionSberuniversity.java_dz3.part1.task5;

public class DayOfWeek {

    private byte dayOfWeek; // added private
    private String nameOfWeek; // added private

    DayOfWeek (byte dayOfWeek, String nameOfWeek) {
        this.dayOfWeek = dayOfWeek;
        this.nameOfWeek = nameOfWeek;
    }

    public byte getDayOfWeek() {
        return dayOfWeek;
    }

    public String getNameOfWeek() {
        return nameOfWeek;
    }

}
