package SolutionSberuniversity.Java_DZ3.Part1.Task4;

public class Solution {
    public static void main(String[] args) {
//        TimeUnit tu = new TimeUnit(12);
//        tu.printTime();
//        TimeUnit tu1 = new TimeUnit(12, 15);
//        tu1.printTime();
        TimeUnit tu2 = new TimeUnit(12, 15, 42);
        tu2.printTime();
        tu2.plusTime(2, 0, 0);
        tu2.printTime();
        tu2.printTimeAMPM();
//        TimeUnit tu3 = new TimeUnit(42, 15, 42);
//        tu3.printTime();
//        TimeUnit tu4 = new TimeUnit(12, 65, 42);
//        tu4.printTime();
//        TimeUnit tu5 = new TimeUnit(12, 15, 68);
//        tu5.printTime();

//        TimeUnit tu6 = new TimeUnit(13);
//        tu6.printTimeAMPM();
//        TimeUnit tu7 = new TimeUnit(13, 12);
//        tu7.printTimeAMPM();
//        TimeUnit tu8 = new TimeUnit(13, 12, 11);
//        tu8.printTimeAMPM();
//        tu8.plusTime(1, 0, 0);
//        tu8.printTimeAMPM();
//        tu8.plusTime(12, 15, 15);
//        tu8.printTimeAMPM();
//        tu8.plusTime();
//        TimeUnit tu9 = new TimeUnit(11);
//        tu9.printTimeAMPM();
//        TimeUnit tu10 = new TimeUnit(11, 10);
//        tu10.printTimeAMPM();

    }
}
