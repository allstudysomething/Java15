package SolutionSberuniversity.java_dz3.part1.task4;

import java.time.DateTimeException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeUnit {

    private int seconds = 0;
    private int minutes = 0;
    private int hours = 0;
    private LocalTime lt; // added private

    public TimeUnit(int hours) {
        this.hours = hours;
    }

    public TimeUnit(int hours, int minutes) {
        this.hours = hours;
        this.minutes = minutes;
    }

    public TimeUnit(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public void printTime() {
        try {
            lt = LocalTime.of(this.hours, this.minutes, this.seconds);
            System.out.println(lt.toString());
        } catch (DateTimeException e) {
            System.out.println(e);
        }
    }

    public void printTimeAMPM() {
        try {
            lt = LocalTime.of(this.hours, this.minutes, this.seconds);
            System.out.println(lt.format(DateTimeFormatter.ofPattern("hh:mm:ss a")));
        } catch (DateTimeException e) {
            System.out.println(e);
        }
    }

    public void plusTime(int hours, int minutes, int seconds) {
        this.hours = lt.plusHours(hours).getHour();
        this.minutes = lt.plusMinutes(minutes).getMinute();
        this.seconds = lt.plusSeconds(seconds).getSecond();
    }

}
