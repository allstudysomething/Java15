package SolutionSberuniversity.Java_DZ3.Part1.Task2_3;

import java.util.Arrays;
import java.util.Comparator;

public class StudentService {

    public Student bestStudent(Student[] students) {

        double highestGrade = students[0].avgGrade();
        int index = 0;
        for (int i = 1; i < students.length; i++) {
            if (students[i].avgGrade() - highestGrade> 0.0001) {
                highestGrade = students[i].avgGrade();
                index = i;
            }
        }

        return students[index];
    }

    public void sortBySurname(Student[] students) {

        if (students.length == 1) return;

//        // first method of sorting
//        boolean isSorted = false;
//        while (!isSorted) {
//            isSorted = true;
//            for (int i = 0; i < students.length - 1; i++) {
//                if (students[i].getSurname().charAt(0) > students[i + 1].getSurname().charAt(0)) {
//                    isSorted = false;
//
//                    Student temp = students[i];
//                    students[i] = students[i + 1];
//                    students[i + 1] = temp;
//                }
//            }
//        }

        // second method of sorting from internet
        Arrays.sort(students, Comparator.comparing(Student::getSurname));

    }
}
