package SolutionSberuniversity.java_dz3.part1.task2_3;

public class Student {

    private String name;
    private String surname;

    private int[] grades = {}; // remove = {0}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public void addGrade(int grade) {

        if (grades.length < 10) {
            int[] tempGrades = new int[grades.length + 1];
            System.arraycopy(grades, 0, tempGrades, 0, grades.length);
            tempGrades[tempGrades.length - 1] = grade;
            grades = tempGrades;
        } else {
            int[] tempGrades = new int[grades.length];
            System.arraycopy(grades, 1, tempGrades, 0, grades.length - 1);
            tempGrades[tempGrades.length - 1] = grade;
            grades = tempGrades;
        }

    }

    public double avgGrade() {
        // add check for empty dimension
        if (grades.length == 0) {
            return 0d;
        }

        int temp = 0;
        for (int i = 0; i < grades.length; i++) {
            temp += grades[i];
        }

        return (double) temp / grades.length;
    }

}
