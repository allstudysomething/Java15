package SolutionSberuniversity.Java_DZ3.Part1.Task7;

public class TriangleChecker {

    public static boolean triangleChecker(double a, double b, double c) {

        if ((a + b > c) && (a + c > b) && (b + c > a)) return true;

        return false;
    }

}
