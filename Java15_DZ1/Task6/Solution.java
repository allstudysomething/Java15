package SolutionSberuniversity.java_dz3.part1.task6;

public class Solution {
    public static void main(String[] args) {
        AmazingString as = new AmazingString(new char[]{'s', 'a', 't', 'u', 'r', 'd', 'a', 'y'});
//        AmazingString as = new AmazingString(new char[]{' ', 'a', 't', 'u', 'r', 'd', 'a', 'y'});
//        AmazingString as = new AmazingString(new char[]{' ', ' ', ' ', 'u', 'r', 'd', 'a', 'y'});
//        AmazingString as = new AmazingString(new char[]{' ', ' ', ' ', ' ', ' ', ' ', ' ', 'y'});
//        System.out.println(as.getaChar(5));
//        System.out.println(as.getaChar(15));
//        System.out.println(as.getLength());
//        as.printLine();

//        System.out.println(as.substring("tur"));
//        System.out.println(as.substring("dai"));
//        System.out.println(as.substring("satur"));
//        System.out.println(as.substring("sadur"));
//        System.out.println(as.substring(new char[]{'t', 'u', 'r'}));
//        System.out.println(as.substring(new char[]{'d', 'a', 'i'}));
//        System.out.println(as.substring(new char[]{'s', 'a', 't', 'u', 'r'}));
//        System.out.println(as.substring(new char[]{'s', 'a', 'd', 'u', 'r'}));

//        as.printLine();
//        as.reverseString();
//        as.printLine();

        as.trimFirst();
        as.printLine();
        as.reverseString();
        as.printLine();


    }
}
