package SolutionSberuniversity.java_dz3.part1.task6;

public class AmazingString {

    /**
     * if int i in method getaChar() < 0 OR > charraray.length then return char space (' ')
     * added method trimFirst after check of issue mentor
     * */

    private char[] ch;
//    String string;

    AmazingString(char[] ch) {
        this.ch = ch;
    }

    AmazingString(String s) {
        this.ch = s.toCharArray();
    }

    public char getaChar(int i) {
        if (i > ch.length || i < 0) return ' ';
        return ch[i - 1];
    }

    public int getLength() {
        return ch.length;
    }

    public void printLine() {
        for (int i = 0; i < ch.length; i++) {
            System.out.print(ch[i]);
        }
        System.out.println();
    }

    public boolean substring(char[] chars) {

        char[] tempChar = chars;
        boolean tempBoolean = false;

        for (int i = 0; i < ch.length; i++) {
            if (ch[i] == tempChar[0] && ch.length - i >= tempChar.length) {
                int tempIndex = i;
                for (int j = 0; j < tempChar.length; j++) {
                    if (tempChar[j] == ch[tempIndex]) {
                        tempBoolean = true;
                    } else {
                        tempBoolean = false;
                        break;
                    }
                    tempIndex++;
                }
            }
        }

        return tempBoolean;
    }

    public boolean substring(String s) {
        return this.substring(s.toCharArray());
    }

    public void reverseString() {
        for (int i = 0, j = ch.length - 1; i < ch.length / 2; i++, j--) {
            char tempLeft = ch[i];
            char tempRight = ch[j];
            ch[i] = tempRight;
            ch[j] = tempLeft;
        }
    }

    public void trimFirst() {
        int countFirstWhiteSpaces = 0;
        boolean isFirstWhiteSpace = false;
        if (ch[0] == ' ') {
            isFirstWhiteSpace = true;
            while (ch[countFirstWhiteSpaces] == ' ') {
                countFirstWhiteSpaces++;
            }
        }
        if (isFirstWhiteSpace && countFirstWhiteSpaces < ch.length) {
            char[] tempChar = new char[ch.length - 1];
            System.arraycopy(ch, countFirstWhiteSpaces, tempChar, 0, tempChar.length - countFirstWhiteSpaces + 1);
            ch = tempChar;
        }
    }

}
