package SolutionSberuniversity.Java_DZ3.Part1.Task1;

public class Cat {

    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }

    public void status() {
        int x = 5;
        while(x > 2) {
            x = (int) (Math.random() * 10);
        }
        if (x == 0) sleep();
        if (x == 1) meow();
        if (x == 2) eat();
    }

}
