package SolutionSberuniversity.java_dz3.part1.task8;

public class solution {
    public static void main(String[] args) throws Exception {

        ATM a1 = null;
        ATM a2 = null;
        ATM a3 = null;
        ATM a4 = null;
        ATM a5 = null;
        ATM a6 = null;

        try {
            a1 = new ATM(61.5, 60.3);;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            a2 = new ATM(61.5, 60.3);;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            a3 = new ATM(61.5, 60.3);;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            a4 = new ATM(-61.5, -60.3);;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            a5 = new ATM(61.5, 60.3);;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            a6 = new ATM(61.5, 60.3);;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println("Total created ATM's is " + ATM.printCountATM());

        // Random test ATM a3
        if (a3 != null) {
            System.out.println(String.format("70 rub = %s usd", a3.convertRubUsd(70)));
            System.out.println(String.format("3.2 usd = %s rub", a3.convertUsdRub(3.2)));
        }

    }
}
