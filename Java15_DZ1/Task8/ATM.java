package SolutionSberuniversity.java_dz3.part1.task8;

public class ATM {

    /* countATM moved down in constructor
    * convertRubUsd and convertUsdRub now returned doubles
    * */

    private static int countATM = 0;
    private double courseRubUsd = 0;
    private double courseUsdRub = 0;

    ATM(double courseRubUsd, double courseUsdRub) throws Exception {
        if (courseRubUsd > 0 && courseUsdRub > 0) {
            this.courseRubUsd = courseRubUsd;
            this.courseUsdRub = courseUsdRub;
        } else throw new Exception("course of convert must be greather 0. ATM number " + (countATM + 1) + " not created");
        countATM++;
    }

    public double convertRubUsd(double rub){
//        System.out.println("convert " + rub + " rub to usd = " + Math.round(rub / courseRubUsd * 100) / 100.0 + " usd");
        return Math.round(rub / courseRubUsd * 100) / 100.0;
    }

    public double convertUsdRub(double usd){
//        System.out.println("convert = " + usd + " usd to rub = " + Math.round(usd * courseUsdRub * 100) / 100.0 + " rub");
        return Math.round(usd * courseUsdRub * 100) / 100.0;
    }

    public static int printCountATM(){
        return countATM;
    }

}
