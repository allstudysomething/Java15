package SolutionSberuniversity.moduleProfessional.dz3.task4;

import java.util.Arrays;

public class Solution {

    public static void main(String[] args) {

        Class<?> clazz = SomeClass.class;
        Class<?> clazz2 = SomeClass2.class;
        Class<?> clazz3 = SomeClass3.class;
        Class<?> clazz4 = SomeClass4.class;

        printInterfaces(clazz);
//        printInterfaces(clazz2);
//        printInterfaces(clazz3);
//        printInterfaces(clazz4);

    }

    private static void printInterfaces(Class<?> someClass) {

//        if (someClass.getInterfaces().length > 0) {
//            System.out.println("Class name = " + someClass.getSimpleName() + ", parent class = " + someClass.getSuperclass().getSimpleName());
//            System.out.println(Arrays.toString(someClass.getInterfaces()));
//        }
//
//        System.out.println("******************************************************");

        if (!someClass.equals(Object.class)) {
//            System.out.println(someClass.getSimpleName());
            System.out.println("Class name = " + someClass.getSimpleName() + ", parent class = " + someClass.getSuperclass().getSimpleName());
            System.out.println(Arrays.toString(someClass.getInterfaces()));
            System.out.println("*******************");
            printInterfaces(someClass.getSuperclass());
        }

    }

}

