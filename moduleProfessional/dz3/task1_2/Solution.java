package SolutionSberuniversity.moduleProfessional.dz3.task1_2;

public class Solution {

    public static void main(String[] args) {

        containsIsLikeAnnotation(TestClassOne.class);
        containsIsLikeAnnotation(TestClassTwo.class);

    }

    public static void containsIsLikeAnnotation(Class<?> clazz) {

        if (clazz.isAnnotationPresent(IsLike.class)) {
            IsLike isLike = clazz.getAnnotation(IsLike.class);
            System.out.println(isLike.IsLiked());
        }

    }

}
