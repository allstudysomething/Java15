package SolutionSberuniversity.moduleProfessional.dz3.task1_2;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(value= RetentionPolicy.RUNTIME)
public @interface IsLike {
    boolean IsLiked() default false;
}
