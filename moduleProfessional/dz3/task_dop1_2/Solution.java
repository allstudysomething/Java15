package SolutionSberuniversity.moduleProfessional.dz3.task_dop1_2;

import java.util.*;

public class Solution {

    // test data dimension
    private static String[] sequences = {
            "(()()())",
            "(()()()",
            null,
            ")()()(",
            "())",
            "[]",
            "[()()]",
            "[())]",
            "[(test)]",
            "[(test2))]",
            "[{(test3)]",
            "[{(test4)}]"
    };

    // Map of mirrored elements
    private static Map<Character, Character> mapMirroredElements = new HashMap<>();

    // Stack for check of valid pairs
    private static Stack<Character> stackCharacters;

    public static void main(String[] args) {

        // add anymore pairs here
        mapMirroredElements.put('(', ')');
        mapMirroredElements.put('{', '}');
        mapMirroredElements.put('[', ']');

        for (String s : sequences) {
            System.out.println(isRightSequence(s));
        }

    }

    private static boolean isRightSequence(String s) {

        if (s == null) { return true; }

        stackCharacters = new Stack<>();

        for(char ch : s.toCharArray()) {
            if (mapMirroredElements.containsKey(ch)) stackCharacters.push(mapMirroredElements.get(ch));
            if (mapMirroredElements.containsValue(ch) && (stackCharacters.isEmpty() || stackCharacters.pop() != ch)) {
                return false;
            }
        }

        return stackCharacters.isEmpty();

    }

}
