package SolutionSberuniversity.moduleProfessional.dz3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Solution {

    public static void main(String[] args) {

        Class<?> clazz = APrinter.class;

        try {

            Method method = clazz.getDeclaredMethod("print", int.class);

//            method.invoke(clazz.newInstance(), 5);
//            method.invoke(clazz.newInstance(), 5d);
            method.invoke(clazz.newInstance(), "5");
//            method.invoke(clazz.newInstance(), 5f);
//            method.invoke(clazz.newInstance(), 5l);
//            method.invoke(clazz.newInstance(), new Integer(5));

            // other
//            Method method1 = clazz.getDeclaredMethod("printX", int.class);
//            method1.invoke(Object.class.newInstance(), 5);

            System.out.println("all good");
        } catch (IllegalAccessException | NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalArgumentException e) {
            System.out.println("error message = " + e.getMessage());
        }

    }

}
