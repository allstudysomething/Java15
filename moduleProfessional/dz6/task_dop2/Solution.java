package SolutionSberuniversity.moduleProfessional.dz6.task_dop2;

public class Solution {

    public static void main(String[] args) {
        int x = 15;
        System.out.println(isNatural(x));
    }

    private static boolean isNatural(int x) {
        if (x > 0) {
            if (x < 4) return true;
            for (int i = 2; i < x; i++) {
                if (x % i == 0) return false;
            }
            return true;
        }
        return false;
    }
}
