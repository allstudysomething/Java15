package SolutionSberuniversity.moduleProfessional.dz6.task_dop1;

public class Solution {
    public static void main(String[] args) {
//        int m = 152;
//        int m = 153;
        int m = 1633;
//        int m = 1634;
        System.out.println(isArmstrongDigital(m));
    }

    private static boolean isArmstrongDigital(int m) {
        String[] str = String.valueOf(m).split("");
        double temp = 0;
        for (int i = 0; i < str.length; i++) {
            temp += Math.pow(Integer.valueOf(str[i]), str.length);
        }
        return (int) temp == m;
    }

}
