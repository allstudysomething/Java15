package SolutionSberuniversity.moduleProfessional.dz2.task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Solution {

    public static void main(String[] args) {

        List<Document> documents = new ArrayList<>();
        documents.add(new Document(14, "Book1", 20 ));
        documents.add(new Document(45, "Book4", 98 ));
        documents.add(new Document(32, "Book1", 83 ));
        documents.add(new Document(23, "Book2", 115 ));

        Map<Integer, Document> newMap = organizeDocuments(documents);
        System.out.println(newMap);

    }

    private static Map<Integer, Document> organizeDocuments(List<Document> documents) {

        Map<Integer, Document> tempMap =
                documents.stream()
                        .collect(Collectors.toMap(Document::getId, Document -> Document));

        return tempMap;

    }

}
