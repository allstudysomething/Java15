package SolutionSberuniversity.moduleProfessional.dz2.task4;

import java.util.Objects;

public class Document {

    private int id;
    private String name;
    private int pageCount;

    public Document(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPageCount() {
        return pageCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Document)) return false;
        Document document = (Document) o;
        return getId() == document.getId() &&
                getPageCount() == document.getPageCount() &&
                Objects.equals(getName(), document.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getPageCount());
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pageCount=" + pageCount +
                '}';
    }

}
