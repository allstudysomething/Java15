package SolutionSberuniversity.moduleProfessional.dz2.task2;

import java.util.Arrays;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {

        try (Scanner sc = new Scanner(System.in)) {

            String s = sc.nextLine();
            String t = sc.nextLine();
            System.out.println(checkAnagramm(s, t));

        }

    }

    private static boolean checkAnagramm(String s, String t) {
        if (s == null || t == null) return false;
        if (s.length() != t.length()) return false;

        char[] tempChar1 = s.toCharArray();
        Arrays.sort(tempChar1);
        char[] tempChar2 = t.toCharArray();
        Arrays.sort(tempChar2);

        for (int i = 0; i < tempChar1.length; i++) {
            if(tempChar1[i] != tempChar2[i]) return false;
        }

        return true;
    }

}
