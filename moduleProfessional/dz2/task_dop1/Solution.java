package SolutionSberuniversity.moduleProfessional.dz2.task_dop1;

import java.util.*;
import java.util.stream.Collectors;

public class Solution {

    private static List<String> preList = new ArrayList<>();
    private static List<String> endList = new ArrayList<>();
    private static int countMax = 0;
    
    public static void main(String[] args) {

        int k = 4;
//        String[] words = {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day"};
////        String[] words = {"theno", "day", "is", "sunny", "theno", "theno", "theno", "sunny", "is", "is", "day",
////                "zay", "zay", "zay", "zay"};
        String[] words = {"day", "day", "day", "day", "zay", "zay", "zay", "zay", "am", "am", "am", "is", "is", "is",
                "clips", "clips", "chips", "chips", "ships", "ships", "shils", "shils"};


        System.out.println(methodFrequencyWords(words, k));

    }

    private static List<String> methodFrequencyWords(String[] words, int k) {

        List<String> tempWords = new ArrayList<>(Arrays.asList(words));

        Set<String> uniqueWords = new HashSet<>(Arrays.asList(words));

        while (!tempWords.isEmpty()) {
            findMaxCount(uniqueWords, tempWords);
            fillMaxFindedCount(uniqueWords, tempWords);
            checkPreListForSort();
            fillEndListFromPreList();
        }

        return endList.stream()
                .limit(k)
                .collect(Collectors.toList());

    }

    private static void fillEndListFromPreList() {
        endList.addAll(preList);
        preList.clear();
    }

    private static void checkPreListForSort() {

        // old Comparator
//        preList.sort(new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                if (o1.charAt(0) == o2.charAt(0)) return 0;
//                if (o1.charAt(0) > o2.charAt(0)) {
//                    return -1;
//                } else {
//                    return 1;
//                }
//            }
//        });


//        // new Comparator
//        preList.sort(new Comparator<String>() {
//        @Override
//        public int compare(String o1, String o2) {
//            int indexMinRavno = 0;
//            for (int i = 0; i < o1.length(); i++) {
//                if (o1.charAt(i) <= o2.charAt(i)) {
//                    indexMinRavno++;
//                } else {
//                    break;
//                }
//            }
//
//            int indexMaxRavno = 0;
//            for (int i = 0; i < o1.length(); i++) {
//                if (o1.charAt(i) >= o2.charAt(i)) {
//                    indexMaxRavno++;
//                } else {
//                    break;
//                }
//            }
//
//            if (indexMaxRavno == indexMinRavno) return 0;
//            if (indexMaxRavno > indexMinRavno) {
//                return -1;
//            } else {
//                return 1;
//            }
//        }
//    });

         // new Comparator 2
        preList.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1.equals(o2)) return 0;
                return o1.compareTo(o2) * -1;
            }
        });


    }

    private static void fillMaxFindedCount(Set<String> uniqueWords, List<String> tempWords) {

        for (String wordSet : uniqueWords) {
            int tempMax = 0;
            for (String wordList : tempWords) {
                if (wordSet.equals(wordList)) tempMax++;
            }
            if (tempMax == countMax) {
                preList.add(wordSet);
                while (tempWords.contains(wordSet)) {
                    tempWords.remove(wordSet);
                }
            }
        }

    }

    private static void findMaxCount(Set<String> uniqueWords, List<String> tempWords) {

        countMax = 0;
        for (String wordSet : uniqueWords) {
            int tempMax = 0;
            for (String wordList : tempWords) {
                if (wordSet.equals(wordList)) tempMax++;
            }
            if (tempMax > countMax) countMax = tempMax;
        }

    }

}
