package SolutionSberuniversity.moduleProfessional.dz2.task1;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    public static void main(String[] args) {

        // check 1
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(2);
        list.add(5);
        System.out.println(uniqueElements(list));

//        // check 2
//        List<String> names = new ArrayList<>();
//        names.add("Vasya");
//        names.add("Petya");
//        names.add("Kolya");
//        names.add("Vasya");
//        names.add("Petya");
//        System.out.println(uniqueElements(names));

//        // check 3
//        DopObjectBook book1 = new DopObjectBook("Romans", "Kolya");
//        DopObjectBook book2 = new DopObjectBook("NoRomans", "Petya");
//        DopObjectBook book3 = new DopObjectBook("Romans", "Kolya");
//        DopObjectBook book4 = new DopObjectBook("Stihi", "Vasya");
//        DopObjectBook book5 = new DopObjectBook("Rasskazi", "Kolya");
//        List<DopObjectBook> books = new ArrayList<>();
//        books.add(book1);
//        books.add(book2);
//        books.add(book3);
//        books.add(book4);
//        books.add(book5);
//        System.out.println(uniqueElements(books));

    }

    public static <T> List<T> uniqueElements(List<T> arrayList) {
        List<T> newList = new ArrayList<>();

        for(T element : arrayList) {
            if (!newList.contains(element)) {
                newList.add(element);
            }
        }

        return newList;
    }

}
