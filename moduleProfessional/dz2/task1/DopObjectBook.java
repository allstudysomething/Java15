package SolutionSberuniversity.moduleProfessional.dz2.task1;

import java.util.Objects;

public class DopObjectBook {

    private String title;
    private String author;

    public DopObjectBook(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DopObjectBook)) return false;
        DopObjectBook that = (DopObjectBook) o;
        return Objects.equals(getTitle(), that.getTitle()) &&
                Objects.equals(getAuthor(), that.getAuthor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getAuthor());
    }

    @Override
    public String toString() {
        return "dopObjectBook{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                '}';
    }

}
