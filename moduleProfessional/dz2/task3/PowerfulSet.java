package SolutionSberuniversity.moduleProfessional.dz2.task3;

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {

    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {

        Set<T> temp1 = new HashSet<T>(set1);
        Set<T> temp2 = new HashSet<T>(set2);

        temp1.retainAll(temp2);
        return temp1;

    }

    public <T> Set<T> union(Set<T> set1, Set<T> set2) {

        Set<T> temp1 = new HashSet<T>(set1);
        Set<T> temp2 = new HashSet<T>(set2);

        temp1.addAll(temp2);
        return temp1;

    }

    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {

        Set<T> temp1 = new HashSet<T>(set1);
        Set<T> temp2 = new HashSet<T>(set2);

        temp1.removeAll(temp2);
        return temp1;

    }

}
