package SolutionSberuniversity.moduleProfessional.dz2.task3;

import java.util.HashSet;
import java.util.Set;

public class Solution {

    public static void main(String[] args) {

        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);

        Set<Integer> set1 = new HashSet<>();
        set1.add(3);
        set1.add(4);
        set1.add(5);
        set1.add(6);

        PowerfulSet powerfulSet = new PowerfulSet();

        System.out.println(powerfulSet.intersection(set, set1));
        System.out.println(powerfulSet.union(set, set1));
        System.out.println(powerfulSet.relativeComplement(set, set1));

    }

}
