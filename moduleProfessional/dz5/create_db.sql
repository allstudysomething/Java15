create database business_flowers;

create schema myschema;

SET search_path TO myschema;

create table clients(
    id SERIAL PRIMARY KEY ,
    name VARCHAR(30) not null ,
    telefon VARCHAR not null ,
    UNIQUE (name, telefon)
);

create table products(
    id SERIAL PRIMARY KEY ,
    name VARCHAR(30) NOT NULL ,
    price INT not null ,
    UNIQUE (name)
);

CREATE TABLE orders(
    id SERIAL PRIMARY KEY ,
    date date NOT NULL ,
    client_id INT references clients(id) ,
    product_id INT references products(id) ,
    count INT NOT NULL CHECK ( count >= 1 AND count<= 1000 ),
    itog INT NOT NULL
);


