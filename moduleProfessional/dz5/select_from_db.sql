SET search_path TO myschema;

-- 1. По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
SELECT orders.id, c.name, c.telefon, p.name, count, itog
FROM orders JOIN clients c on orders.client_id = c.id
    JOIN products p on orders.product_id = p.id;

-- 2. Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
SELECT orders.id, c.name, p.name, count, itog FROM orders JOIN clients c on orders.client_id = c.id
    JOIN products p on orders.product_id = p.id
WHERE client_id = 4 AND EXTRACT(MONTH FROM date) = (SELECT EXTRACT(MONTH FROM max(date)) FROM orders);

-- 3. Найти заказ с максимальным количеством купленных цветов, вывести их название и количеств
SELECT p.name, count
FROM orders JOIN clients c on orders.client_id = c.id
    JOIN products p on orders.product_id = p.id
WHERE count = (SELECT max(count) FROM orders);

-- 4. Вывести общую выручку (сумму золотых монет по всем заказам) за все время
SELECT SUM(itog) FROM orders;