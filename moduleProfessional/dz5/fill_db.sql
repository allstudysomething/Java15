SET search_path TO myschema;

INSERT INTO clients(name, telefon)
VALUES ('Masha', '89181112233'),
       ('Dasha', '89182221133'),
       ('Pasha', '89183332211'),
       ('Vasha', '89184445577'),
       ('Sasha', '89185551212');

INSERT INTO products(name, price)
VALUES ('Roza', 100),
       ('Liliya', 50),
       ('Romashka', 25);

INSERT INTO orders(date, client_id, product_id, count, itog)
VALUES ('2023-01-12', 1, 1, 7, 700),
       ('2023-01-20', 1, 2, 5, 250),
       ('2023-02-25', 1, 3, 110, 2750),
       ('2023-01-15', 2, 1, 7, 700),
       ('2023-01-18', 2, 2, 50, 2500),
       ('2023-02-14', 2, 3, 11, 275),
       ('2023-01-17', 3, 1, 7, 700),
       ('2023-01-16', 3, 2, 5, 250),
       ('2023-02-19', 3, 3, 11, 275),
       ('2023-01-17', 4, 1, 70, 7000),
       ('2023-02-16', 4, 2, 5, 250),
       ('2023-02-11', 4, 3, 11, 275),
       ('2023-01-18', 5, 1, 7, 700),
       ('2023-02-23', 5, 2, 5, 250),
       ('2023-02-27', 5, 3, 11, 275);

-- // try insert not valid values test
-- INSERT INTO orders(client_id, product_id, count, itog)
-- VALUES (1, 1, 7000, 700);