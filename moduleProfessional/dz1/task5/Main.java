package SolutionSberuniversity.moduleProfessional.dz1.task5;
// add import
import java.util.Scanner;

// bez IDE slojno i dolgo
public class Main {

    public static void main(String[] args) {
        // add try catch
        try {
            int n = inputN();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Успешный ввод!");
    }

    // add throws Exception
    private static int inputN() throws Exception {
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        // maybe (!(n < 100 && n > 0))
        if (!(n < 100 && n > 0)) {
            throw new Exception("Неверный ввод");
        }
        return n;
    }

}
