package SolutionSberuniversity.moduleProfessional.dz1.task4;

import java.util.ArrayList;

public class Solution {

    public static void main(String[] args) {
        ArrayList<MyEvenNumber> arrr = new ArrayList<>();
        try {
            // first way
//            MyEvenNumber myEvenNumber = new MyEvenNumber(6);
//            System.out.println(myEvenNumber.getNumber());
//            MyEvenNumber myEvenNumber1 = new MyEvenNumber(7);
//            System.out.println(myEvenNumber1.getNumber());

            // second way
            arrr.add(new MyEvenNumber(4));
            arrr.add(new MyEvenNumber(5));
            arrr.add(new MyEvenNumber(6));
            arrr.add(new MyEvenNumber(7));
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(arrr.size());


    }

}
