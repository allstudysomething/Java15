package SolutionSberuniversity.moduleProfessional.dz1.task4;

public class MyEvenNumber {

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    private Integer number;

    public MyEvenNumber(Integer number) throws Exception {
        if (number % 2 == 0) {
            this.number = number;
        } else throw new Exception();
    }

}
