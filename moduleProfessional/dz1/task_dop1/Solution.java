package SolutionSberuniversity.moduleProfessional.dz1.task_dop1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// bez IDE slojno i dolgo
public class Solution {

    public static void main(String[] args) {

        List<Integer> arrs = new ArrayList<>();

        try ( Scanner sc = new Scanner(System.in) ) {
            int length = sc.nextInt();
            for (int i = 0; i < length; i++) {
                arrs.add(sc.nextInt());
            }

            findTwoMaxDigits(arrs);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    private static void findTwoMaxDigits(List<Integer> arrs) {
        List<Integer> arrs1 = new ArrayList<>();

        for (int i = 0; i < 2; i++) {

            Integer temp = Integer.MIN_VALUE;
            for (Integer integer : arrs) {
                if (temp < integer) temp = integer;
            }

            arrs1.add(temp);
            arrs.remove(temp);
        }

        arrs1.stream().forEach(s -> System.out.print(s + " "));
    }

}
