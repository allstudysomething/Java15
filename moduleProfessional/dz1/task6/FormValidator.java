package SolutionSberuniversity.moduleProfessional.dz1.task6;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FormValidator {

    public static boolean checkName(String name) {
        if (isNull(name)) return false;
        return (name.length() <= 20 && name.length() >= 2) && name.matches("[A-Z][a-z]+");
    }

    public static boolean checkBirthdate(String name) {
        if (isNull(name) || !name.matches("(0?[1-9]|[12][0-9]|3[01])\\.(0?[1-9]|1[012])\\.((19|20)\\d\\d)"))
            return false;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy");
        LocalDate localDate = LocalDate.parse(name, formatter);
        LocalDate localDateNow = LocalDate.now();
        LocalDate localDate1970 = LocalDate.parse("01.01.1970", formatter);
        if (! (localDate.isEqual(localDate1970) || localDate.isEqual(localDateNow))) {
            if ( !(localDate.isBefore(localDateNow) && localDate.isAfter(localDate1970)) ) return false;
        }

        return true;
    }

    public static boolean checkGender(String name) {
        if (isNull(name)) return false;
        return (name.equals(Gender.Male.toString()) || name.equals(Gender.Female.toString()));
    }

    public static boolean checkHeight(String name) {
        if (isNull(name)) return false;
        double d;

        try {
            d = Double.parseDouble(name);
            if (d < 0) throw new NumberFormatException(d + " < 0");
            return true;
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    private static boolean isNull(String name) {
        return name == null;
    }

}
