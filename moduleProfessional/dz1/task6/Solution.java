package SolutionSberuniversity.moduleProfessional.dz1.task6;

public class Solution {

    public static void main(String[] args) {
        // first method check
//        System.out.println(FormValidator.checkName(null));
//        System.out.println(FormValidator.checkName("asker"));
//        System.out.println(FormValidator.checkName("Asker"));
//        System.out.println(FormValidator.checkName("ASKER"));

        // second method check
//        System.out.println(FormValidator.checkBirthdate(null));
//        System.out.println(FormValidator.checkBirthdate("01.01.1975"));
//        System.out.println(FormValidator.checkBirthdate("01.01/1975"));
//        System.out.println(FormValidator.checkBirthdate("01.017975"));
//        System.out.println(FormValidator.checkBirthdate("01.01.1975"));
//        System.out.println(FormValidator.checkBirthdate("41.01.1975"));
//        System.out.println(FormValidator.checkBirthdate("11.21.1975"));
//        System.out.println(FormValidator.checkBirthdate("11.02.1835"));
//        System.out.println(FormValidator.checkBirthdate("11.2.1975"));
//        System.out.println(FormValidator.checkBirthdate("1.2.1975"));
//        System.out.println(FormValidator.checkBirthdate("29.2.1975"));
//        System.out.println(FormValidator.checkBirthdate("9.11.1975"));
//        System.out.println(FormValidator.checkBirthdate("9.11.2075"));

        // third method check
//        System.out.println(FormValidator.checkGender("Male"));
//        System.out.println(FormValidator.checkGender("Female"));
//        System.out.println(FormValidator.checkGender("male"));
//        System.out.println(FormValidator.checkGender("Dale"));

        // fourth method check
//        System.out.println(FormValidator.checkHeight("152"));
//        System.out.println(FormValidator.checkHeight("a152"));
//        System.out.println(FormValidator.checkHeight("-152.11"));

    }

}
