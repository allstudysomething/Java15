package SolutionSberuniversity.moduleProfessional.dz1.task2;

public class MyUncheckedException extends NumberFormatException {

    public MyUncheckedException(String message) {
        super(message);
    }

}
