package SolutionSberuniversity.moduleProfessional.dz1.task3;

import java.io.*;

public class Solution {

    public static void main(String[] args) {

        try(
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("c:\\testjava1\\input.txt"))));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File("c:\\testjava1\\output.txt"))))
        ) {
            while (br.ready()) {
                String newLine = br.readLine().toUpperCase();
                bw.write(newLine + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
