package SolutionSberuniversity.moduleProfessional.dz1.task_dop2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// bez IDE slojno i dolgo
public class Solution {

    public static void main(String[] args) {
        List<Integer> arrs = new ArrayList<>();

        try (Scanner sc = new Scanner(System.in)) {
            int length = sc.nextInt();
            for (int i = 0; i < length; i++) {
                arrs.add(sc.nextInt());
            }
            int digit = sc.nextInt();

            int index = -1;
            for (int i = 0; i < arrs.size(); i++) {
                if (digit == arrs.get(i)) {
                    index = i;
                }
            }

            System.out.println(index);
        } catch (Exception e) {
            e.getMessage();
        }

    }

}
