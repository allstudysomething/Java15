package SolutionSberuniversity.moduleProfessional.dz1.task1;

public class Cat {

    private String name;
    private boolean hungry;
    private boolean sleep;

    Cat(String name) {
        this.name = name;
    };

    public void catWalk() throws MyCheckedException {
        if (isHungry() || isSleep()) {
            System.out.println("Ready to walk");
        } else throw new MyCheckedException("Need to eat or sleep");
    }

    private boolean isHungry() {
        return hungry;
    }

    private boolean isSleep() {
        return sleep;
    }

}
