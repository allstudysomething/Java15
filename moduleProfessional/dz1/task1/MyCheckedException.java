package SolutionSberuniversity.moduleProfessional.dz1.task1;

public class MyCheckedException extends ArithmeticException {

    public MyCheckedException(String message) {
        super(message);
    }

}
