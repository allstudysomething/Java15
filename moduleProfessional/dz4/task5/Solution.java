package SolutionSberuniversity.moduleProfessional.dz4.task5;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {

    public static void main(String[] args) {

        List<String> arrs = new ArrayList<>();
        arrs.add("abc");
        arrs.add("def");
        arrs.add("ghj");

        System.out.println(arrs.stream()
                .map(e -> e.toUpperCase())
                .collect(Collectors.joining(",")));

    }

}
