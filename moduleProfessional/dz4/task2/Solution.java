package SolutionSberuniversity.moduleProfessional.dz4.task2;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    public static void main(String[] args) {

        List<Integer> integerList = new ArrayList<>();
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        integerList.add(5);

        System.out.println(integerList.stream()
                .reduce((a, b) -> a * b).get());

    }

}
