package SolutionSberuniversity.moduleProfessional.dz4.task4;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    public static void main(String[] args) {

        List<Double> arrs = new ArrayList<>();
        arrs.add(12.56);
        arrs.add(5.05);
        arrs.add(17.25);
        arrs.add(-5.15);
        arrs.add(-15.07);

        arrs.stream()
                .sorted((o1, o2) -> o1.compareTo(o2) * -1)
                .forEach(System.out::println);

    }

}
