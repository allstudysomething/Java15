package SolutionSberuniversity.moduleProfessional.dz4.task6;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Solution {

    public static void main(String[] args) {

        // fill Set of Sets
        Set<Set<Integer>> sets = new HashSet<>();
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        Set<Integer> set2 = new HashSet<>();
        set2.add(5);
        set2.add(6);
        Set<Integer> set3 = new HashSet<>();
        set3.add(9);
        set3.add(10);
        sets.add(set1);
        sets.add(set2);
        sets.add(set3);

        // Solution
        System.out.println("Set of sets (Set<Set<Integer>>) = " + sets);
        Set<Integer> integerSet = sets.stream().flatMap(Collection::stream).collect(Collectors.toSet());
        System.out.println("New Set (Set<Integer>) = " + integerSet);

    }

}
