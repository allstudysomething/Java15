package SolutionSberuniversity.moduleProfessional.dz4.task1;

import java.util.stream.IntStream;

public class Solution {

    public static void main(String[] args) {

        System.out.println(IntStream
                .range(1, 101)
                .filter(e -> e % 2 == 0)
                .sum()
        );

    }

}
