package SolutionSberuniversity.moduleProfessional.dz4.task_dop1;

public class Solution {

    public static void main(String[] args) {

        String[][] strings = {
                {"cat", "cats"},
                {"cat", "scat"},
                {"cats", "cat"},
                {"scat", "cat"},
                {"cat", "cut"},
                {"cat", "bat"},
                {"cat", "cab"},
                {"cat", "nut"}
        };


        for (String[] str : strings) {
            int onlyOne = 0;

            if (str[0] != null && str[1] != null) {
                if (checkCanAddSymbol(str)) onlyOne++;
                if (checkCanDelSymbol(str)) onlyOne++;
                if (checkCanChangeSymbol(str)) onlyOne++;
            }

            System.out.println(onlyOne == 1);

        }

    }

    private static boolean checkCanChangeSymbol(String[] str) {
        char[] ch = str[0].toCharArray();
        char[] ch1 = str[1].toCharArray();

        int countValid = 0;
        if (ch.length == ch1.length) {
            for (int i = 0; i < ch.length; i++) {
                if (ch[i] == ch1[i]) countValid++;
            }
        }
        return ch.length - countValid == 1;
    }

    private static boolean checkCanAddSymbol(String[] str) {
        String s = str[0];
        String s1 = str[1];

        return ((s.length() == s1.length() - 1) && s1.contains(s));
    }

    private static boolean checkCanDelSymbol(String[] str) {
        String s = str[0];
        String s1 = str[1];

        return (s.length() - 1 == s1.length()) && s.contains(s1);
    }

}
