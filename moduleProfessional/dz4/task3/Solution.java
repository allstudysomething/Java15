package SolutionSberuniversity.moduleProfessional.dz4.task3;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    public static void main(String[] args) {

        List<String> arrs = new ArrayList<>();
        arrs.add("abc");
        arrs.add("");
        arrs.add("");
        arrs.add("bce");
        arrs.add("acc");

        System.out.println(arrs.stream()
                .filter(e -> !e.isEmpty())
                .count());

    }

}
