package SolutionSberuniversity.java_dz3.part3.task3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        List<List<Integer>> arrColumnLine = new ArrayList<>();

        Scanner sc = new Scanner(System.in);
        int columns = sc.nextInt();
        int lines = sc.nextInt();
        sc.close();

        // fill List<List<Integer>> (lines with sum i + j)
        for (int i = 0; i < lines; i++) {
            List<Integer> arri = new ArrayList<>();
            for (int j = 0; j < columns; j++) {
                arri.add(i + j);
            }
            arrColumnLine.add(arri);
        }

        // print List<List<Integer>>
        for (List<Integer> tempArrayList : arrColumnLine) {
            for (Integer temInteger : tempArrayList) {
                System.out.print(temInteger + " ");
            }
            System.out.println();
        }

    }

}
