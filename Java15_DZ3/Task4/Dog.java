package SolutionSberuniversity.java_dz3.part3.task4;

public class Dog {

    private final String name;
    private double AVGGrade = 0;
    private final int asd = 3;

    Dog(String name) {
        this.name = name;
    }

    public double getAVGGrade() {
        return AVGGrade;
    }

    public void setAVGGrade(double grade) {
        this.AVGGrade = grade;
    }

    public String getName() {
        return name;
    }

}
