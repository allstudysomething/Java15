package SolutionSberuniversity.java_dz3.part3.task4;

import java.util.*;

public class Konkurs {

    private final List<Participant> listOfParticipants = new ArrayList<>();

    public int getlistOfParticipantsSize() {
        return listOfParticipants.size();
    }

    public void addParticipant(Participant participant) {
        listOfParticipants.add(participant);
    }

    public Participant getParticipant(String name) {
        for (int i = 0; i < listOfParticipants.size(); i++) {
            if (listOfParticipants.get(i).getName().equals(name)) {
                return listOfParticipants.get(i);
            }
        }

        System.out.println("Participant not finded here");
        return new Participant("");
    }

    public Participant getParticipant(int index) {
        return listOfParticipants.get(index);
    }

    public void setParticipantDog(String name, Dog dog) {
        Participant tempParticipant = getParticipant(name);
        if (!tempParticipant.getName().equals("")) {
            tempParticipant.setParticipantDog(dog);
        }
    }

//    public void printAllParticipantDogsAGVGrades() {
//        System.out.println("Print All Participants Dogs AVG Grades");
//        for (int i = 0; i < listOfParticipants.size(); i++) {
//            System.out.println(listOfParticipants.get(i).getName() + " : " + listOfParticipants.get(i).getParticipantDog().getName()
//                    + " : " + listOfParticipants.get(i).getParticipantDog().getAVGGrade());
//        }
//    }

    // edit to toString instead of method printAllParticipantDogsAGVGrades
    @Override
    public String toString() {
        System.out.println("Print All Participants Dogs AVG Grades");
        for (int i = 0; i < listOfParticipants.size(); i++) {
            System.out.println(listOfParticipants.get(i).getName() + " : " + listOfParticipants.get(i).getParticipantDog().getName()
                    + " : " + listOfParticipants.get(i).getParticipantDog().getAVGGrade());
        }
        return "";
    }

    public void printNfirst(int count) {
        if (count > listOfParticipants.size()) {
            System.out.println("WRONG : Count " + count + " > Participants count");
            return;
        }

        System.out.println("Result first " + count + " Participants Dogs AVG Grades");

        List<Double> listOfGrades = new ArrayList<>();
        List<Participant> listOfParticipantsDuplicate = new ArrayList<>(listOfParticipants);

//        for (int i = 0; i < listOfParticipants.size(); i++) {
//            listOfGrades.add(listOfParticipantsDuplicate.get(i).getParticipantDog().getAVGGrade());
//        }

        // edit to forEach
        for(Participant p : listOfParticipantsDuplicate) {
            listOfGrades.add(p.getParticipantDog().getAVGGrade());
        }

        Collections.sort(listOfGrades);
        Collections.reverse(listOfGrades);

        for (int i = 0; i < count; i++) {
            double currentGrade = listOfGrades.get(i);
            for (int j = 0; j < listOfParticipants.size(); j++) {
                if(currentGrade == listOfParticipants.get(j).getParticipantDog().getAVGGrade()) {
                    System.out.println(listOfParticipants.get(j).getName() + ": " + listOfParticipants.get(j).getParticipantDog().getName()
                            + ", " + listOfParticipants.get(j).getParticipantDog().getAVGGrade());
                    break;
                }
            }
        }

    }

}