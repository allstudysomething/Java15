package SolutionSberuniversity.java_dz3.part3.task4;

public class Participant {

    private final String name;
    private Dog participantDog;

    public Dog getParticipantDog() {
        return participantDog;
    }

    public void setParticipantDog(Dog participantDog) {
        this.participantDog = participantDog;
    }

    Participant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
