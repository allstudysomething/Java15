package SolutionSberuniversity.java_dz3.part3.task4;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        Konkurs konkurs1 = new Konkurs();
        Scanner sc = new Scanner(System.in);
        int countOfParticipants = sc.nextInt(); sc.nextLine();
        // Duplicat for test
//        Konkurs konkurs1 = new Konkurs();
//        int countOfParticipants = 4;

// ***************************************************************************************

        // fill Participants
        for (int i = 0; i < countOfParticipants; i++) {
            konkurs1.addParticipant(new Participant(sc.nextLine()));
        }
//        // Duplicat for test
//        konkurs1.addParticipant(new Participant("ivan"));
//        konkurs1.addParticipant(new Participant("nikolay"));
//        konkurs1.addParticipant(new Participant("anna"));
//        konkurs1.addParticipant(new Participant("darya"));

// ***************************************************************************************

        // fill Dogs for Participants
        for (int i = 0; i < konkurs1.getlistOfParticipantsSize(); i++) {
            konkurs1.getParticipant(i).setParticipantDog(new Dog(sc.nextLine()));
        }
//        // Duplicat for test
//        konkurs1.setParticipantDog("ivan", new Dog("juchka"));
//        konkurs1.setParticipantDog("nikolay", new Dog("knopka"));
//        konkurs1.setParticipantDog("anna", new Dog("cesar"));
//        konkurs1.setParticipantDog("darya", new Dog("dobryash"));

// ***************************************************************************************

        // fill AVG Grades for Dogs of Participants
        for (int i = 0; i < countOfParticipants; i++) {
            String[] tempArrString = sc.nextLine().split(" ");
            double tempGrade = 0;
            for (int j = 0; j < 3; j++) {
                tempGrade += Integer.parseInt(tempArrString[j]) * 1.0;
            }
            tempGrade = (int)(tempGrade * 10 / 3) * 1.0 / 10;
            konkurs1.getParticipant(i).getParticipantDog().setAVGGrade(tempGrade);
        }
//        // Duplicat for test
//        double tempGrade = 6.6;
//        konkurs1.getParticipant("ivan").getParticipantDog().setAVGGrade(tempGrade);
//        double tempGrade1 = 7.6;
//        konkurs1.getParticipant("nikolay").getParticipantDog().setAVGGrade(tempGrade1);
//        double tempGrade2 = 5.0;
//        konkurs1.getParticipant("anna").getParticipantDog().setAVGGrade(tempGrade2);
//        double tempGrade3 = 9.0;
//        konkurs1.getParticipant("darya").getParticipantDog().setAVGGrade(tempGrade3);

// ***************************************************************************************

        // print result
//        konkurs1.printAllParticipantDogsAGVGrades();
        System.out.println(konkurs1);
        System.out.println();
        konkurs1.printNfirst(3);

    }

}
