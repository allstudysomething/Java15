package SolutionSberuniversity.java_dz3.part3.task2;

class Bed extends Mebel {

    private String name;
    private String color;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    void setName(String name) {
        this.name = name;
    }

    @Override
    String getColor() {
        return this.color;
    }

    @Override
    void setColor(String color) {
        this.color = color;
    }

}
