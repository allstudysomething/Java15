package SolutionSberuniversity.java_dz3.part3.task2;

abstract class Mebel {

    abstract String getName();
    abstract void setName(String name);
    abstract String getColor();
    abstract void setColor(String color);

//    String name;
//    String color;
//
//    public String getName() {
//        return this.name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getColor() {
//        return this.color;
//    }
//
//    public void setColor(String color) {
//        this.color = color;
//    }

}
