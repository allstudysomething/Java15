package SolutionSberuniversity.java_dz3.part3.task1;

public class Bat extends Animal implements Flying {

    private WayOfBirth wayOfBirth = WayOfBirth.layEggs;

    @Override
    public void printFlyingSpeed() {
        System.out.println("Bat fly speed is 30");
    }

    @Override
    WayOfBirth getWayOfBirth() {
        return this.wayOfBirth;
    }

}
