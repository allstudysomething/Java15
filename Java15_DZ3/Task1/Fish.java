package SolutionSberuniversity.java_dz3.part3.task1;

public class Fish extends Animal {

    private WayOfBirth wayOfBirth = WayOfBirth.sendCaviar;

    @Override
    public WayOfBirth getWayOfBirth() {
        return this.wayOfBirth;
    }

}
