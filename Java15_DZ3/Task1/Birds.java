package SolutionSberuniversity.java_dz3.part3.task1;

public class Birds extends Animal {

    private WayOfBirth wayOfBirth = WayOfBirth.layEggs;

    @Override
    WayOfBirth getWayOfBirth() {
        return this.wayOfBirth;
    }

}
