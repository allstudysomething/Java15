package SolutionSberuniversity.java_dz3.part3.task1;

public class GoldFish extends Animal implements Swimming {

    private WayOfBirth wayOfBirth = WayOfBirth.sendCaviar;

    @Override
    public void printSwimmingSpeed() {
        System.out.println("GoldFish swim speed is 5");
    }

    @Override
    WayOfBirth getWayOfBirth() {
        return this.wayOfBirth;
    }

}
