package SolutionSberuniversity.java_dz3.part3.task1;

public class Eagle extends Animal implements Flying {

    private WayOfBirth wayOfBirth = WayOfBirth.layEggs;

    @Override
    public void printFlyingSpeed() {
        System.out.println("Eagle fly speed is 60");
    }

    @Override
    WayOfBirth getWayOfBirth() {
        return this.wayOfBirth;
    }

}
