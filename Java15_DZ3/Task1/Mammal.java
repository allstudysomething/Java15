package SolutionSberuniversity.java_dz3.part3.task1;

public class Mammal extends Animal {

    private WayOfBirth wayOfBirth = WayOfBirth.jivorod;

    @Override
    public WayOfBirth getWayOfBirth() {
        return this.wayOfBirth;
    }

}
