package SolutionSberuniversity.java_dz3.part3.task1;

public class Dolphin extends Animal implements Swimming {

    private WayOfBirth wayOfBirth = WayOfBirth.jivorod;

    @Override
    public void printSwimmingSpeed() {
        System.out.println("Dolphin swim speed is 35");
    }

    @Override
    WayOfBirth getWayOfBirth() {
        return this.wayOfBirth;
    }

}
