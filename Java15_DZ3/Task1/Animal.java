package SolutionSberuniversity.java_dz3.part3.task1;

abstract class Animal {

//    Enum wayOfBirth;
    public final void eat() { System.out.println("Animal eat"); } // eat now is not abstract
    public final void sleep() { System.out.println("Animal sleep"); } //sleep now is not abstract
    abstract WayOfBirth getWayOfBirth();

}









